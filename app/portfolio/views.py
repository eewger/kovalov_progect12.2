import os
from flask import render_template, redirect, url_for, request, abort
from datetime import datetime
from .data import certificats
from . import portfolio

mySkills = [
    {
        "skillName": "HTML",
        "level": "skillful",
       
    },
    {
        "skillName": "CSS",
        "level": "skillful",
       
    },
    {
        "skillName": "Python",
        "level": "beginer",
        
    },
    {
        "skillName": "GIT",
        "level": "beginer",
        
    },
    {
        "skillName": "SQL",
        "level": "skillful",
       
    }
] 


@portfolio.route('/')
def home():
    osInfo = os.environ.get('OS')
    agent = request.user_agent
    time = datetime.now().strftime("%H:%M:%S")
    
    return render_template('home.html', certificats=certificats, agent=agent, time=time, osInfo=osInfo)

@portfolio.route('/skills/<int:id>')
@portfolio.route('/skills')
def skills(id=None):
    
    if id:
        if id > len(mySkills):
            abort(404)
        else:
            index = id - 1
            skill = mySkills[index]
            return render_template('skill.html', skill=skill, id=id)    
    else:
        return render_template('skills.html', mySkills=mySkills)
    
    
